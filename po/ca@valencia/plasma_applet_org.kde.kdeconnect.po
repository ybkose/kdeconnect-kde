# Translation of plasma_applet_org.kde.kdeconnect.po to Catalan (Valencian)
# Copyright (C) 2014-2023 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021.
# Josep M. Ferrer <txemaq@gmail.com>, 2018, 2019, 2020, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: kdeconnect-kde\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-22 00:41+0000\n"
"PO-Revision-Date: 2023-06-08 08:30+0200\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca@valencia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Generator: Lokalize 20.12.0\n"

#: package/contents/ui/Battery.qml:25
#, kde-format
msgid "%1% charging"
msgstr "%1% de càrrega"

#: package/contents/ui/Battery.qml:25
#, kde-format
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/Battery.qml:25
#, kde-format
msgid "No info"
msgstr "Sense informació"

#: package/contents/ui/Connectivity.qml:40
#, kde-format
msgid "Unknown"
msgstr "Desconeguda"

#: package/contents/ui/Connectivity.qml:50
#, kde-format
msgid "No signal"
msgstr "Sense senyal"

#: package/contents/ui/DeviceDelegate.qml:56
#, kde-format
msgid "File Transfer"
msgstr "Transferència de fitxers"

#: package/contents/ui/DeviceDelegate.qml:57
#, kde-format
msgid "Drop a file to transfer it onto your phone."
msgstr "Deixeu anar un fitxer per a transferir-lo cap al telèfon."

#: package/contents/ui/DeviceDelegate.qml:93
#, kde-format
msgid "Virtual Display"
msgstr "Pantalla virtual"

#: package/contents/ui/DeviceDelegate.qml:146
#, kde-format
msgctxt "Battery charge percentage"
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/DeviceDelegate.qml:168
#, kde-format
msgid "Please choose a file"
msgstr "Seleccioneu un fitxer"

#: package/contents/ui/DeviceDelegate.qml:177
#, kde-format
msgid "Share file"
msgstr "Compartix el fitxer"

#: package/contents/ui/DeviceDelegate.qml:192
#, kde-format
msgid "Send Clipboard"
msgstr "Envia el porta-retalls"

#: package/contents/ui/DeviceDelegate.qml:211
#, kde-format
msgid "Ring my phone"
msgstr "Fes sonar el meu telèfon"

#: package/contents/ui/DeviceDelegate.qml:229
#, kde-format
msgid "Browse this device"
msgstr "Navega per este dispositiu"

#: package/contents/ui/DeviceDelegate.qml:246
#, kde-format
msgid "SMS Messages"
msgstr "Missatges SMS"

#: package/contents/ui/DeviceDelegate.qml:267
#, kde-format
msgid "Remote Keyboard"
msgstr "Teclat remot"

#: package/contents/ui/DeviceDelegate.qml:288
#, kde-format
msgid "Notifications:"
msgstr "Notificacions:"

#: package/contents/ui/DeviceDelegate.qml:296
#, kde-format
msgid "Dismiss all notifications"
msgstr "Descarta totes les notificacions"

#: package/contents/ui/DeviceDelegate.qml:343
#, kde-format
msgid "Reply"
msgstr "Respon"

#: package/contents/ui/DeviceDelegate.qml:353
#, kde-format
msgid "Dismiss"
msgstr "Descarta"

#: package/contents/ui/DeviceDelegate.qml:366
#, kde-format
msgid "Cancel"
msgstr "Cancel·la"

#: package/contents/ui/DeviceDelegate.qml:380
#, kde-format
msgctxt "@info:placeholder"
msgid "Reply to %1…"
msgstr "Respon a %1…"

#: package/contents/ui/DeviceDelegate.qml:398
#, kde-format
msgid "Send"
msgstr "Envia"

#: package/contents/ui/DeviceDelegate.qml:424
#, kde-format
msgid "Run command"
msgstr "Executa una ordre"

#: package/contents/ui/DeviceDelegate.qml:432
#, kde-format
msgid "Add command"
msgstr "Afig una ordre"

#: package/contents/ui/FullRepresentation.qml:58
#, kde-format
msgid "No paired devices"
msgstr "No hi ha cap dispositiu aparellat"

#: package/contents/ui/FullRepresentation.qml:58
#, kde-format
msgid "Paired device is unavailable"
msgid_plural "All paired devices are unavailable"
msgstr[0] "El dispositiu aparellat no està disponible"
msgstr[1] "No hi ha disponible cap dels dispositius aparellats"

#: package/contents/ui/FullRepresentation.qml:60
#, kde-format
msgid "Install KDE Connect on your Android device to integrate it with Plasma!"
msgstr ""
"Instal·leu KDE Connect al dispositiu Android per a integrar-lo amb Plasma!"

#: package/contents/ui/FullRepresentation.qml:64
#, kde-format
msgid "Pair a Device..."
msgstr "Aparella un dispositiu..."

#: package/contents/ui/FullRepresentation.qml:76
#, kde-format
msgid "Install from Google Play"
msgstr "Instal·la des de Google Play"

#: package/contents/ui/FullRepresentation.qml:86
#, kde-format
msgid "Install from F-Droid"
msgstr "Instal·la des del F-Droid"

#: package/contents/ui/main.qml:59
#, kde-format
msgid "KDE Connect Settings..."
msgstr "Configureu KDE Connect..."
